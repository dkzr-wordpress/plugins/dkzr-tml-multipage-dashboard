<?php
/**
 * Plugin Name: DKZR TML Multipage Dashboard
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/tml-multipage-dashboard
 * Description: Configures mulitple settings.
 * Version: 2.0
 * Author: Joost de Keijzer <j@dkzr.nl>
 * Text Domain: dkzr-managed
 * Domain Path: languages
 * License: GPL2
 */

add_action( 'plugins_loaded', function() {
  if ( class_exists( 'Theme_My_Login_Extension' ) && function_exists( 'tml_register_extension' ) ) {
    require_once __DIR__ . '/includes/tml-multipage-dashboard.php';
    $tml_ext = new Dkzr_Tml_Multipage_Dashboard( __FILE__ );

    add_action( 'tml_activate_' . $tml_ext->get_slug(), 'tml_flush_rewrite_rules' );
    add_action( 'tml_deactivate_' . $tml_ext->get_slug(), 'tml_flush_rewrite_rules' );

    tml_register_extension( $tml_ext, [] );
  }
} );
