<?php
class Dkzr_Tml_Multipage_Dashboard extends Theme_My_Login_Extension {
  protected $name = 'dkzr-multipage-dashboard';
  protected $title = 'DKZR Theme My Login Multipage Dashboard';
  protected $version = '1.0';

  protected $homepage_url;
  protected $documentation_url;
  protected $support_url;

  protected function add_actions() {
    add_action( 'init', [ $this, 'add_rewrite_rules' ] );
  }

  public function add_rewrite_rules() {
    if ( ! tml_use_permalinks() || ! ( $action = tml_get_action( 'dashboard' ) ) ) {
      return;
    }

    $url = 'index.php?action=' . $action->get_name();
    if ( $page = tml_action_has_page( $action ) ) {
      $url .= '&pagename=' . get_page_uri( $page ) . '/$matches[1]';
    }
    add_rewrite_rule( tml_get_action_slug( $action ) . '/(.+)$', $url, 'top' );
  }
}
