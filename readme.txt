=== DKZR TML multipage dashboard ===
Contributors: 		joostdekeijzer
Requires at least: 	6.0
Tested up to: 		6.0
Stable tag: 		1.0
Requires PHP:		8.0
License: 			GPLv2 or later
License URI: 		http://www.gnu.org/licenses/gpl-2.0.html

Theme My Login extension that requires the whole dashboard pagetree to have a valid user login.

== Installation ==

Use WordPress' Add New Plugin feature, searching "Duplicate Post", or download the archive and:

1. Unzip the archive on your computer
2. Upload `duplicate-post` directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Go to Settings -> Duplicate Post and customize behaviour as needed

== Version history ==

= 1.0 (2022) =
* Initial version
